import sqlite3
from tarantool import Connection

##### sqlite #####
def get_tasks_list():
    conn = sqlite3.connect('todo.db')
    c = conn.cursor()
    c.execute("SELECT id, task FROM todo WHERE status LIKE '1'")
    result = c.fetchall()
    return result

def insert_task(task):
    conn = sqlite3.connect('todo.db')
    c = conn.cursor()

    query = "INSERT INTO todo (task,status) VALUES ('%s',1)" %task
    c.execute(query)
    conn.commit()

    c.execute("SELECT last_insert_rowid()")
    new_id = c.fetchone()[0]
    c.close

    return new_id

def edit_task(no, request=None, task=None, status=None):
    if request == 'save':
        conn = sqlite3.connect('todo.db')
        c = conn.cursor()
        query = "UPDATE todo SET task = '%s', status = '%s' WHERE id LIKE '%s'" % (task,status,no)
        c.execute(query)
        conn.commit()
    else:
        conn = sqlite3.connect('todo.db')
        c = conn.cursor()
        query = "SELECT task, status FROM todo WHERE id LIKE '%d'" % int(no)
        c.execute(query)
        cur_data = c.fetchone()
        return cur_data


##### tarantool #####
def get_tasks_list_tnt():
    db = Connection("127.0.0.1", 3301)
    space = db.space("todo")
    tasks = space.select()

    # tasks = db.select("todo")
    return tasks


def insert_task_tnt(task, no):
    db = Connection("127.0.0.1", 3301)
    space = db.space("todo")
    space.insert((int(no), task, 1))

    # db.insert("phones", (no, task))


def edit_task_tnt(no, request=None, task=None, status=None):
    db = Connection("127.0.0.1", 3301)
    space = db.space("todo")

    if request == 'save':
        query = space.update(int(no), [2, '=', task])
    else:
        task = space.select(int(no))
        return task
