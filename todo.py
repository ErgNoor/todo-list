import sqlite3
from bottle import route, run, debug, template, request, redirect
from db_commands import get_tasks_list, insert_task, edit_task, get_tasks_list_tnt,\
                        insert_task_tnt, edit_task_tnt

@route('/todo')
def show_todo_list():
    # tasks_list = get_tasks_list()
    tasks_list = get_tasks_list_tnt()

    output = template('make_table', rows=get_tasks_list())
    return output

@route('/new', method='GET')
def new_item():

    if request.GET.get('save','').strip():

        new = request.GET.get('task', '').strip()
        new_id = insert_task(new)

        # redirect("todo")
        return '<p>The new task was inserted into the database, the ID is %s</p>' %new_id
    else:
        return template('new_task.tpl')


@route('/edit/:no', method='GET')
def edit_item(no):

    if request.GET.get('save','').strip():
        edit = request.GET.get('task','').strip()
        status = request.GET.get('status','').strip()

        if status == 'open':
            status = 1
        else:
            status = 0

        edit_task(no, 'save', edit, status)

        # redirect("/todourl")
        return '<p>The item number %d was successfully updated</p>' % int(no)
    else:
        return template('edit_task', old = edit_task(no=no), no = no)

debug(True)
run(reloader=True)
